<?php

ob_start();
session_start();
require_once('config.php');
	
if (isset($_SESSION['user'])) {
	header("Location: index.php");
	exit;
}

$errors = array();

if (isset($_POST['submit'])) {
	$name = mysqli_real_escape_string($con, $_POST['name']);
	$username = mysqli_real_escape_string($con, $_POST['username']);
	$password_1 = mysqli_real_escape_string($con, $_POST['password_1']);
	$password_2 = mysqli_real_escape_string($con, $_POST['password_2']);

	if (empty($name)) { array_push($errors, "Silakan masukkan nama Anda."); }
	if (empty($username)) { array_push($errors, "Silakan masukkan username Anda."); }
	
	if ($password_1 != $password_2) {
		array_push($errors, "Password tidak cocok");
	}

	if (count($errors) == 0) {
		$password = md5($password_1);

		$query = "INSERT INTO users (name, username, password, role_id) VALUES('$name', '$username', '$password', 3);";
		$insert = mysqli_query($con, $query);

		if ($insert) {
			$_SESSION['user'] = $username;
			$_SESSION['success'] = "You are now logged in";
				
			header("Location: index.php");
		} else {
			array_push($errors, mysqli_error($con));
		}

	}

}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Daftar</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/css/ionicons.min.css">
  <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
  <link rel="stylesheet" href="assets/css/blue.css">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="index.php"><b>Penilaian</b>Siswa</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Daftar akun baru</p>

    <form action="register.php" method="post">

    	<?php if (count($errors) > 0) { ?>
    		<div class="alert alert-danger" role="alert">
				  <?php foreach ($errors as $error) { ?>
				  	<p><?= $error ?></p>
				  <?php } ?>
				</div>
    	<?php } ?>

      <div class="form-group has-feedback">
        <input type="text" name="name" class="form-control" placeholder="Nama Lengkap">
      </div>
      <div class="form-group has-feedback">
        <input type="text" name="username" class="form-control" placeholder="Username">
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password_1" class="form-control" placeholder="Kata sandi">
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password_2" class="form-control" placeholder="Ulangi kata sandi">
      </div>
      <div class="row">
        <div class="col-xs-4">
          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
      </div>
    </form>

    Sudah punya akun? <a href="login.html" class="text-center">Masuk</a>
  </div>
  <!-- /.form-box -->
</div>
</body>
</html>
