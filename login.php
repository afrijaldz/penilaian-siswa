<?php
ob_start();
session_start();
require_once('config.php');

if ($con->connect_error) {
    die("Connection failed: " . $con->connect_error);
} 
	
if (isset($_SESSION['user'])) {
	header("Location: index.php");
	exit;
}

$errors = array();

if (isset($_POST['username']) && isset($_POST['password'])) {
  $username = mysqli_real_escape_string($con, $_POST['username']);
  $password = mysqli_real_escape_string($con, $_POST['password']);

  if (empty($username)) { array_push($errors, "Username is required"); }
  if (empty($password)) { array_push($errors, "Password is required"); }

  if (count($errors) == 0) {
    $password = md5($password);
    $query = "SELECT * FROM users WHERE username='$username' AND password='$password'";

    $stmt = $con->prepare("SELECT id, username, password FROM users WHERE username= ? AND password= ?");
    $stmt->bind_param("ss", $username, $password);
    $stmt->execute();
    $res = $stmt->get_result();
    $stmt->close();

    $row = mysqli_fetch_array($res, MYSQLI_ASSOC);

    $count = $res->num_rows;
    if ($count == 1 && $row['password'] == $password) {
        $_SESSION['user'] = $row['id'];
        header("Location: index.php");
    } elseif ($count == 1) {
        array_push($errors, 'Password salah');
    } else array_push($errors, 'User tidak ditemukan');
  }
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Masuk</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/css/ionicons.min.css">
  <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
  <link rel="stylesheet" href="assets/css/blue.css">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="index.php"><b>Penilaian</b>Siswa</a>
  </div>
  <div class="login-box-body">
    <p class="login-box-msg">Silakan login untuk masuk ke Web</p>

    <form action="login.php" method="POST">
      
      <?php if (count($errors) > 0) { ?>
        <div class="alert alert-danger" role="alert">
          <?php foreach ($errors as $error) { ?>
            <p><?= $error ?></p>
          <?php } ?>
        </div>
      <?php } ?>

      <div class="form-group has-feedback">
        <input name="username" type="text" class="form-control" placeholder="Nama pengguna">
      </div>
      <div class="form-group has-feedback">
        <input name="password" type="password" class="form-control" placeholder="Kata sandi">
      </div>
      <div class="form-group has-feedback">
        <button type="submit" class="btn btn-primary btn-block btn-flat">Masuk</button>
      </div>
    </form>
    Belum punya akun? <a href="register.php" class="text-center">Daftar</a>

  </div>
	</div>
</body>
</html>
